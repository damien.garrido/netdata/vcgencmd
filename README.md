# Introduction
This netdata plugin uses the vcgencmd executable to retrieve Raspberry Pi temperature and voltages.

# Prerequisites
* You will need the libraspberrypi-bin and of course the netdata packages.
* You will need to add the ```netdata``` user in the ```video```group:

```bash
usermod -aG video netdata
```

# Installation
* Copy the file ```vcgencmd.char.py``` under the ```/usr/lib.../netdata/python.d/``` folder.
* Copy the file ```vcgencmd.conf``` under the ```/etc/netdata/python.d/``` folder.
* Add the following line at the end of file ```/etc/netdata/python.d.conf```:
```yaml
vcgencmd: yes
```

* Restart the netdata service.
