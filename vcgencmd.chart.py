# -*- coding: utf-8 -*-
# Description: vcgencmd netdata python.d module
# Author: Damien Garrido

import re
from subprocess import Popen, PIPE

from bases.FrameworkServices.SimpleService import SimpleService

# default module values (can be overridden per job in `config`)
#update_every = 2
priority = 60000
retries = 60

ORDER = ['temperature', 'voltage']
CHART = {
	'temperature' : {
		'options' : ['temperature', u"Temperature in Celsius", "degrees Celsius", 'temperature', 'vcgencmd.temperature', 'line'],
		'lines' : []
	},
	'voltage' : {
		'options' : ['voltage', "Voltage in Volts", "volts", 'voltage', 'vcgencmd.volts', 'line'],
		'lines' : []
	}
}

class Service(SimpleService):

	kind_regex = {
		'temperature': re.compile("^temp=(\d+\.\d)'C"),
		'voltage': re.compile("^volt=(\d+\.\d{4})V")
	}

	kind_ratio = {
		'temperature': 10,
		'voltage': 10000
	}

	def __init__(self, configuration=None, name='Raspberry Pi sensors'):
		SimpleService.__init__(self, configuration=configuration, name=name)
		self.command = 'vcgencmd'
		self.order = ORDER
		self.definitions = CHART

	def run_command(self, args, stderr=False):
		command = [self.command] + args 
		try:
			p = Popen(command, stdout=PIPE, stderr=PIPE)
		except Exception as error:
			self.error("Executing command {command} resulted in error: {error}".format(command=command, error=error))
			return None
		lines = list()
		output = p.stderr if stderr else p.stdout
		for line in output:
			try:
				lines.append(line.decode('utf-8'))
			except TypeError:
				continue
		return lines or None

	def check_run_command(self, args, stderr=False):
		result = self.run_command(args)
		if result[0].startswith("VHCI initialization failed"):
			raise ValueError("Unable to run command {command} {args}: {result}. Please check if the user running netdata (usually user 'netdata') is in the group 'video'.".format(command=self.command, args=args, result=result))
		return result

	def check(self):
		try:
			for command in self.configuration['commands']:
				kind = command['kind']
				if kind not in Service.kind_regex:
					self.error("Unknown kind {kind}".format(kind=kind))
					return False
				result = self.run_command(command['args'])
				if len(result) != 1 or not Service.kind_regex[kind].match(result[0]):
					self.error("Illegal return of command {command} {args}: {result}".format(command=self.command, args=command['args'], result=result))
					return False
		except Exception as error:
			self.error("While checking: {error}".format(error=error))
			return False
		return True

	def _get_data(self):
		result = dict()
		try:
			for command in self.configuration['commands']:
				kind = command['kind']
				if command['id'] not in self.charts[kind]:
					self.charts[kind].add_dimension([command['id'], command['legend_name'], 'absolute', 1, Service.kind_ratio[kind]])
				command_result = self.run_command(command['args'])
				if len(command_result) != 1:
					raise ValueError("Command {command} {args} returned more than one line: {result}".format(command=self.command, args=args, result=command_result))
				match = Service.kind_regex[kind].match(command_result[0])
				if not match:
					raise ValueError("Command {command} {args} returned an invalid result: {result}".format(command=self.command, args=args, result=command_result))
				result[command['id']] = int(float(match.group(1)) * Service.kind_ratio[kind])
			return result
		except (ValueError, AttributeError) as error:
			self.error("An error occured while retrieving data: {error}".format(error=error))
			return result or None
